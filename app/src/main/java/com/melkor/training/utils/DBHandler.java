package com.melkor.training.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by pablo.quiroga on 3/5/2018.
 */

public class DBHandler extends SQLiteOpenHelper{
    public static final String DB_NAME = "DBregistros";
    public static final int DB_VERSION = 1;
    public static final String DB_Tabla = "registros";
    public static final String COLUMNA_ID = "_id";
    public static final String COLUMNA_FECHA = "fecha";

    private static final String sqlcreate = "create table "
            + DB_Tabla +"(" + COLUMNA_ID
            + " integer primary key autoincrement, "
            + COLUMNA_FECHA + " date not null );";


    public DBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlcreate); //crea la DDBB
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {
    //aqui se realizan cambios a la DDBB cuando se cambia de version
        db.execSQL("DROP TABLE IF EXISTS registros"); //elimina la tabla preexistente
        db.execSQL(sqlcreate); //crea la nueva version de la tabla

    }
}
