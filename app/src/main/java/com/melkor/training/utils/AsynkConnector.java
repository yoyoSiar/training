package com.melkor.training.utils;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Pablo Daniel Quiroga on 1/3/2018.
 */

public class AsynkConnector extends AsyncTask<Void, String, String>{
    private final String resource = "https://jsonplaceholder.typicode.com/users";
    private Callback callback;

    public AsynkConnector(Callback c) {
        this.callback = c;
    }

    @Override
    protected void onPreExecute() {
        callback.starting();
    }
    @Override
    protected String doInBackground(Void... voids) {
        return doConnection();

    }
    @Override
    protected void onProgressUpdate(String... values) {
        callback.update();
    }
    @Override
    protected void onPostExecute(String s) {
        callback.completed(s);
    }


    private String doConnection(){
        String retorno = null;
        try{
            //Crea la conexion
            URL servicio = new URL(resource);
            HttpsURLConnection conexion = (HttpsURLConnection) servicio.openConnection();

            //comprueba la conexion
            if(conexion.getResponseCode() == 200){
                //Convierte el InputStream en String
                InputStream is = conexion.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder result = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                retorno = result.toString();

                reader.close();
                is.close();
            }else{
                //Quiza mostrar un Toast para este caso
            }
            conexion.disconnect();
        }
        catch (MalformedURLException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }

        return retorno;
    }
}
