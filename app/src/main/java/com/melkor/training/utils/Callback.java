package com.melkor.training.utils;

/**
 * Created by Pablo Daniel Quiroga on 1/3/2018.
 */

public interface Callback {
    public void starting();
    public void completed(String res);
    public void completedWithErrors(Exception e);
    public void update();
}
