package com.melkor.training.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.melkor.training.R;
import com.melkor.training.entities.User;

import java.util.List;

/**
 * Created by Pablo Daniel Quiroga on 1/3/2018.
 */

public class Adaptador  extends ArrayAdapter<User>{

    public Adaptador(Context context, List<User> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null){
            view = LayoutInflater.from(getContext()).inflate(
                    R.layout.users_lista,parent,false);
        }
        User user = getItem(position);
        String usuario = user.getUsername();
        String mail = user.getEmail();

        TextView textUser = (TextView)view.findViewById(R.id.txtuser);
        TextView textMail = (TextView)view.findViewById(R.id.txtmail);

        textUser.setText(usuario);
        textMail.setText(mail);

        return view;
    }
}
