package com.melkor.training.activities;

import android.content.DialogInterface;
import android.os.Build;
import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.melkor.training.R;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener{

    EditText editText;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        editText = (EditText)findViewById(R.id.edtNombre);
        button = (Button)findViewById(R.id.btnSend);

        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String nombre = editText.getText().toString();
        String text = "Bienvenido " + nombre;
        AlertDialog.Builder builder;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            builder = new android.app.AlertDialog.Builder(RegistroActivity.this, R.style.Theme_AppCompat_DayNight);
        }else {
            builder = new android.app.AlertDialog.Builder(RegistroActivity.this);
        }

        builder.setTitle("Bienvenido")
                .setMessage(text)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }
}
