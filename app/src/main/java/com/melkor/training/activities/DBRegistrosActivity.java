package com.melkor.training.activities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.melkor.training.R;
import com.melkor.training.utils.DBHandler;

import java.util.Date;

public class DBRegistrosActivity extends AppCompatActivity implements View.OnClickListener{

    DBHandler registros;
    SQLiteDatabase db;
    Button button;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dbregistros);

        button = (Button)findViewById(R.id.btn_registrar);
        textView = (TextView)findViewById(R.id.txtcontador);

        //abre la DDBB de registros en modo lectura
        registros = new DBHandler(this);
        db = registros.getWritableDatabase(); //abre la DDBB en modo escritura

        button.setOnClickListener(this);

        contar();
    }

    @Override
    public void onClick(View v) {
        Date fecha = new Date();
        String format = fecha.toString();
        /*SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date()).toString();*/

        if(db != null){
            ContentValues registro = new ContentValues();
            registro.put("fecha", format);
            db.insert("registros", null, registro);

        }
        contar();
        Toast.makeText(this, format/*"Se cargo fecha y hora indicada"*/,
                Toast.LENGTH_SHORT).show();
    }

    private void contar(){
        Cursor c = db.rawQuery("SELECT fecha FROM registros",null);
        int retorno = c.getCount();
        textView.setText(Integer.toString(retorno));
    }

}
