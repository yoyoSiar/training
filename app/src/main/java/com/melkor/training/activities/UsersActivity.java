package com.melkor.training.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.melkor.training.R;
import com.melkor.training.entities.User;
import com.melkor.training.utils.Adaptador;
import com.melkor.training.utils.AsynkConnector;
import com.melkor.training.utils.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UsersActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    ListView listView;
    List<User> listado;
    Adaptador adaptador;
    ProgressBar bar;

    User seleccionado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        listado = new ArrayList<>();
        listView = (ListView)findViewById(R.id.lvusuarios);
        bar = (ProgressBar)findViewById(R.id.progressbar);
        adaptador = new Adaptador(this, new ArrayList<User>());
        listView.setAdapter(adaptador);

        getData();

        listView.setOnItemClickListener(this);
    }

    private void getData(){
        AsynkConnector conectar = new AsynkConnector(new Callback() {
            @Override
            public void starting() {

            } //al pedo
            @Override
            public void completed(String res) {
                parseResponse(res);
                adaptador.clear();

                if(listado != null && !listado.isEmpty()){
                    adaptador.addAll(listado);
                }
                bar.setVisibility(View.GONE);
            }
            @Override
            public void completedWithErrors(Exception e) {} //al pedo de momento
            @Override
            public void update() {} //otro mas al pedo
        });
        conectar.execute();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        seleccionado = listado.get(i);
        Intent intent = new Intent(UsersActivity.this, UserDetailActivity.class);
        intent.putExtra("usuario", seleccionado);
        startActivity(intent);
    }

    private void parseResponse(String s){

        try {
            JSONArray arrayJson = new JSONArray(s);
            for (int i = 0; i < arrayJson.length(); i++) {
                JSONObject objetoJson = arrayJson.getJSONObject(i);
                User u = new User(objetoJson);
                listado.add(u);
            }
        }catch (JSONException e){}
    }
}
