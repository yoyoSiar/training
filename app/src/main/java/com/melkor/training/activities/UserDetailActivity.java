package com.melkor.training.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.melkor.training.R;
import com.melkor.training.entities.User;

public class UserDetailActivity extends AppCompatActivity {

    TextView txtname;
    TextView txtusername;
    TextView txtmail;
    TextView txtstreet;
    TextView txtsuite;
    TextView txtcity;
    TextView txtphone;
    TextView txtwebsite;
    TextView txtcompanyname;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);

        txtname = (TextView)findViewById(R.id.txtname);
        txtusername = (TextView)findViewById(R.id.txtusername);
        txtmail = (TextView)findViewById(R.id.txtmail);
        txtstreet = (TextView)findViewById(R.id.txtstreet);
        txtsuite = (TextView)findViewById(R.id.txtsuite);
        txtcity = (TextView)findViewById(R.id.txtcity);
        txtphone = (TextView)findViewById(R.id.txtphone);
        txtwebsite = (TextView)findViewById(R.id.txtwebsite);
        txtcompanyname = (TextView)findViewById(R.id.txtcompanyname);

        user = (User)getIntent().getExtras().getSerializable("usuario");

        getData();
    }

    private void getData(){
        txtname.setText(user.getName());
        txtusername.setText(user.getUsername());
        txtmail.setText(user.getEmail());
        txtstreet.setText(user.getAddress().getStreet());
        txtsuite.setText(user.getAddress().getSuite());
        txtcity.setText(user.getAddress().getCity());
        txtphone.setText(user.getPhone());
        txtwebsite.setText(user.getWebsite());
        txtcompanyname.setText(user.getCompany().getName());
    }
}
